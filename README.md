## Netdata

This role installs and configures netdata exporter.

Tested on debian 10.

## Role parameters

| name                            | value         | optionnal | default value   | description                               |
| --------------------------------|---------------|-----------|-----------------|-------------------------------------------|
| netdata_listen_host             | ip addr       | yes       | 127.0.0.1       | netdata exporter listen address           |
| netdata_configurations          | see below     | yes       | []              | templates file for configuration plugins  |

The variable `netdata_configurations` is an array of dict containing keys: plugin, template.
```yaml
      netdata_configurations:
        - { plugin: python, template: tests/templates/haproxy.conf }
        - { plugin: go, template: tests/templates/apache.conf }
```

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/ansible-roles3/netdata.git
  scm: git
  version: v1.2
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: netdata
      netdata_listen_host: 0.0.0.0
      netdata_configurations:
        - { plugin: python, template: tests/templates/haproxy.conf }
        - { plugin: go, template: tests/templates/apache.conf }
```

## Tests

[tests/tests_netdata](tests/tests_netdata)
